f = open("msg.txt", "r")
msg = f.readline()
f.close()

def binarize(msg):
    """Converts the message to binary"""
    msg = msg.replace("x", "0")
    msg = msg.replace("X", "1")
    msg = msg.replace("d", "0")
    msg = msg.replace("D", "1")
    return msg

msg = binarize(msg)

def binary_to_ascii(binary_string):
    """Converts a binary string to ASCII"""

    # Split the binary string into groups of 8 characters
    binary_groups = [binary_string[i:i+8] for i in range(0, len(binary_string), 8)]

    # Convert each binary group to decimal and then to ASCII
    ascii_characters = ''.join([chr(int(group, 2)) for group in binary_groups])
    
    return ascii_characters

ascii_result = binary_to_ascii(msg)

#write this into a file
f = open("extracted.c", "w")
f.write(ascii_result)
f.close()

print("output written to extracted.c")