# private-conversation

Welcome to the "private-conversation" challenge, where you find yourself in the role of a cryptanalyst facing an intriguing encrypted message.
Scenario

In the midst of your cryptographic investigations, you stumble upon a fragment of a conversation that appears to be encoded in a highly unusual and complex manner. The content of this conversation could potentially hold significant information or secrets.

Your challenge is to decrypt the message and reveal its content. The fate of uncovering valuable information lies in your decryption skills. Can you decipher the message and unveil the hidden message within?

<div style="text-align:center"><img src="image.png" width="70%" /></div>

## solving steps

0. the message to decypher is made only of 4 characters $\{x,X,d,D\}$, which make a reference to the [XD emoticon](https://it.wikipedia.org/wiki/Emoticon) symbolyzing a laugh.
0. since this message is composed only by 4 symbols, and not more, it could be an alphabetic or polyalphabetic cypher. It is however impossible to know the lenght of the encoded chunch (2,4,5 sumbols ?). So i started as lenght 1, so considering character by character. I therefore translated every character to binary, mapping the lowercase letters to 0 and the uppercase letter to 1.
0. this way i obtained a binary string. Looking for the flag, i tried to decode it in ASCII characters with the `binary_to_ascii` function, obtaining some c code, provided in `extracted.txt`
0. executing that c code prints the flag: `ping{why_so_serious_XD}` 🥳

## contrast with original solution

the official solution actually used pais of characters (therefore pairs of bits) in this fashion:

- "xd" -> 00
- "xD" -> 01
- "Xd" -> 10
- "XD" -> 11

interestingly there is no practical difference in the result using this technique or the one i used
